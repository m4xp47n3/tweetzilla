visualizeBar("data/6040data1.tsv","#barDiv6040","Classifier Performance [60 - 40]"); visualizePie("data/6040data1.tsv","#barDiv6040");
visualizeBar("data/8020data1.tsv","#barDiv8020","Classifier Performance [80 - 20]"); visualizePie("data/8020data1.tsv","#barDiv8020");
visualizeBar("data/MobileOS_Final_StatisticsNB.tsv","#barDiv1", "Naive Bayes"); visualizePie("data/MobileOS_Final_StatisticsNB.tsv","#barDiv1");
visualizeBar("data/MobileOS_Final_StatisticsCNB.tsv","#barDiv2", "Complimentary Naive Bayes"); visualizePie("data/MobileOS_Final_StatisticsCNB.tsv","#barDiv2");

function visualizeBar(dataset, div, title){
var margin = {top: 80, right: 180, bottom: 80, left: 180},
    width = 600 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1, .3);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(8, "%");

var svg = d3.select(div).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.tsv(dataset, type, function(error, data) {
  x.domain(data.map(function(d) { return d.name; }));
  y.domain([0, d3.max(data, function(d) { return d.value; })]);

  svg.append("text")
      .attr("class", "title")
      .attr("x", x(data[0].name))
      .attr("y", -26)
      .text(title);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .selectAll(".tick text")
      .call(wrap, x.rangeBand());

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  svg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.name); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); });
});

function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}


};

function type(d) {
  d.value = +d.value;
  return d;
}

function visualizePie(dataset, div){
    d3.tsv(dataset, type, function(error, data) {
      		var pie = d3.layout.pie();
        	var w = 300;
        	var h = 300;
        	var outerRadius = w/2;
        	var innerRadius = 60;
        					
        	var svg = d3.select(div)
        				.append("svg")
        				.attr("width",w)
        				.attr("height",h);
        	
        	var arc = d3.svg.arc()
        					.innerRadius(innerRadius)
        					.outerRadius(outerRadius);
        				
        	var arcs = svg.selectAll("g.arc")
        					.data(pie(data.map(function(d) { return d.value; })))
        					.enter()
        					.append("g")
        					.attr("class","arc")
        					.attr("transform","translate("+outerRadius+","+outerRadius+")");
        	var color = d3.scale.category10();

        	var labels = data.map(function(d) { return d.name; });

        	arcs.append("path")
        		.attr("fill",function(d,i){return color(i);})
        		.attr("d",arc);
        		
        	arcs.append("text")
        		.attr("transform", function(d) {
    					    return "translate(" + arc.centroid(d) + ")";
				    })
 			   .attr("text-anchor", "middle")
 			   .text(function(d,i) {
 			   		   return labels[i];
   					 });

		});
        	
};