        	var dataset = [ 70, 20, 8, 2 ];	
        	var smartPhones = ["Android", "iPhone", "WindowsPhone", "Blackberry"];
        	var pie = d3.layout.pie();
        	var w = 400;
        	var h = 400;
        	var outerRadius = w/2;
        	var innerRadius = 0;
        	var arc = d3.svg.arc()
        					.innerRadius(innerRadius)
        					.outerRadius(outerRadius);
        	var svg = d3.select("#pieDiv")
        				.append("svg")
        				.attr("width",w)
        				.attr("height",h);
        	var arcs = svg.selectAll("g.arc")
        					.data(pie(dataset))
        					.enter()
        					.append("g")
        					.attr("class","arc")
        					.attr("transform","translate("+outerRadius+","+outerRadius+")");
        	var color = d3.scale.category10();
        	
        	arcs.append("path")
        		.attr("fill",function(d,i){return color(i);})
        		.attr("d",arc);
        		
        	arcs.append("text")
        		.attr("transform", function(d) {
    					    return "translate(" + arc.centroid(d) + ")";
				    })
 			   .attr("text-anchor", "middle")
 			   .text(function(d,i) {
 				       return smartPhones[i] + " " + d.value + "%";
   					 });