displayScatter();
function displayScatter(){
        	//Dynamic, random dataset
			//var dataset = [];
			/*d3.csv("usa_mobile_compn_output.csv", function(data) {
  				dataset = data.map(function(d) {console.log(+d["score"]); return [ +d["TweetID"], +d["score"] ]; });
			});*/
			var dataset = [];
			var numDataPoints = 50;
			var xRange = Math.random() * 1000;
			var yRange = Math.random() * 1000;
			for (var i = 0; i < numDataPoints; i++) {
			    var newNumber1 = Math.round(Math.random() * xRange);
			    var newNumber2 = Math.round(Math.random() * yRange);
			    dataset.push([newNumber1, newNumber2]);
			}
              
        	var w = 600;
        	var h = 400;
        	var svg = d3.select("#scatterDiv")
        				.append("svg")
        				.attr("width",w)
        				.attr("height",h);
        	var padding = 30;
        	var xScale = d3.scale.linear()
        							.domain([0, d3.max(dataset, function(d){return d[0];})])
        							.range([padding,w - padding]);
        							
        	var yScale = d3.scale.linear()
        							.domain([0, d3.max(dataset, function(d){return d[1];})])
        							.range([h - padding,padding]);
        							
        	var rScale = d3.scale.linear()
            			         .domain([0, d3.max(dataset, function(d) { return d[1]; })])
                     			.range([2, 5]);
        	
        	var xAxis = d3.svg.axis()
        					.scale(xScale)
        					.orient("bottom")
        					.ticks(5);
        	
        	var yAxis = d3.svg.axis()
        					.scale(yScale)
        					.orient("left")
        					.ticks(5);
        					
        	svg.selectAll("circle")
        		.data(dataset)
        		.enter()
        		.append("circle")
        		.transition()    // <-- Transition #1
   				.duration(1000)
			   .each("start", function() {
           			d3.select(this)
		             .attr("fill", "magenta")
       			      .attr("r", 7);
   					})
        		.attr("cx", function(d) {
        						return xScale(d[0]);
   					})
   				.attr("cy", function(d) {
        						return yScale(d[1]);
   					})
			    .attr("r", function(d) {
  									  return rScale(d[1]);
						})
				.transition()    // <-- Transition #2
   				.duration(1000)
			    .attr("fill", "black");
            
        	svg.append("g")
        		.attr("class","axis")
        		.attr("transform","translate(0,"+(h-padding)+")")
        		.call(xAxis);
        	
        	svg.append("g")
			    .attr("class", "axis")
			    .attr("transform", "translate(" + padding + ",0)")
			    .call(yAxis);
			//Update x-axis
			svg.select(".x.axis")
    			.transition()
			    .duration(1000)
			    .call(xAxis);

			//Update y-axis
			svg.select(".y.axis")
			    .transition()
			    .duration(1000)
			    .call(yAxis);
	
			//On click, update with new data			
			/*d3.select("#scatterDiv")
				.on("click", function() {

					//New values for dataset
					var numValues = dataset.length;						 		//Count original length of dataset
					var maxRange = Math.random() * 1000;						//Max range of new values
					dataset = [];  						 				 		//Initialize empty array
					for (var i = 0; i < numValues; i++) {				 		//Loop numValues times
						var newNumber1 = Math.floor(Math.random() * maxRange);	//New random integer
						var newNumber2 = Math.floor(Math.random() * maxRange);	//New random integer
						dataset.push([newNumber1, newNumber2]);					//Add new number to array
					}
					
					//Update scale domains
					xScale.domain([0, d3.max(dataset, function(d) { return d[0]; })]);
					yScale.domain([0, d3.max(dataset, function(d) { return d[1]; })]);

					//Update all circles
					svg.selectAll("circle")
					   .data(dataset)
					   .transition()
   					   .duration(1000)
					   .attr("cx", function(d) {
					   		return xScale(d[0]);
					   })
					   .attr("cy", function(d) {
					   		return yScale(d[1]);
					   });

				});*/
};