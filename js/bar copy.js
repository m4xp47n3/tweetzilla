		d3.json("analysis1.json", function(data) {
				var dataset = [];
				dataset.push(data.analysis[0].summary.ccip);
				var jsonData = data.analysis[0].summary;
				var labels = [];
				for(var label in jsonData)
				{
					labels.push(label);
				}
				dataset.push(data.analysis[0].summary.icip);
				visualize(dataset,labels);
			});
			
		function visualize(dataset, labels){
			var w = 500;
        	var h = 300;
        	var padding = 20;
        	
			var margin = {top: 20, right: 20, bottom: 30, left: 40};
		    var width = w - margin.left - margin.right;
		    var height = h - margin.top - margin.bottom;

			var x = d3.scale.ordinal()
				    .rangeRoundBands([0, width], .1);

			var y = d3.scale.linear()
				    .range([height, 0]);

			var xAxis = d3.svg.axis()
					    .scale(x)
					    .orient("bottom");

			var yAxis = d3.svg.axis()
					    .scale(y)
					    .orient("left")
					    .ticks(10, "%");

			var svg = d3.select("#barDiv").append("svg")
						    .attr("width", width + margin.left + margin.right)
						    .attr("height", height + margin.top + margin.bottom)
							.append("g")
						    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		   svg.append("g")
		      .attr("class", "x axis")
		      .attr("transform", "translate(0," + height + ")")
		      .call(xAxis);

		  svg.append("g")
		      .attr("class", "y axis")
		      .call(yAxis)
			  .append("text")
		      .attr("transform", "rotate(-90)")
		      .attr("y", 6)
		      .attr("dy", ".71em")
		      .style("text-anchor", "end")
		      .text("Frequency");

		  svg.selectAll(".bar")
		     .data(dataset)
		     .enter().append("rect")
		     .attr("class", "bar")
             .attr("x", function(d, i) { return x(labels[i]); })
		     .attr("width", x.rangeBand())
	         .attr("y", function(d) { return y(d); })
	         .attr("height", function(d) { return height - y(d); });
        					
        }
        	
        	
        		