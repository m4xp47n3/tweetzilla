animateLogo();

function animateLogo(){
	var dataset = [];
	var numDataPoints = 50;
	var xRange = Math.random() * 1000;
	var yRange = Math.random() * 1000;
	for (var i = 0; i < numDataPoints; i++) {
			    var newNumber1 = Math.round(Math.random() * xRange);
			    var newNumber2 = Math.round(Math.random() * yRange);
			    dataset.push([newNumber1, newNumber2]);
	}
					
	d3.select("#logo")
	  .selectAll("svg")
	  .data(dataset)
      .transition()
      .duration(1000)
      .each("start", function() {
           					d3.select(this)
		             			.attr("fill", "red")
   						})
	  .transition()    // <-- Transition #2
	  .duration(1000)
      .attr("fill", "#3BA9EE");
	
};